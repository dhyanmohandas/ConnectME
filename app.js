var app = angular.module('BookShelf', ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'components/home/home.html',
            controller: 'homeController'
        })
    $urlRouterProvider.otherwise('/home');
}]);
